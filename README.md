Please find the most recent version of the software at https://gitlab.rrz.uni-hamburg.de/groups/corpus-services/corpus-services

# Introduction

The HZSK Corpus Services project bundles functionality used for maintenance, curation, conversion, and visualization of corpus data at the HZSK and in the project INEL.  

# Gitlab artifacts

The latest compiled .jar can be found here: 
https://gitlab.rrz.uni-hamburg.de/hzsk-open-access/hzsk-corpus-services-release/-/jobs/artifacts/release/browse?job=compile_withmaven

# Compilation

To use the validator for HZSK corpora, compile it using `mvn clean compile assembly:single`.
or use a pregenerated artifact form gitlab (see Gitlab artifacts). 

# Usage

The usable functions can be found in the help output:

`java -jar hzsk-corpus-services-0.2-jar-with-dependencies.jar -h`

# Metadata

PID: http://hdl.handle.net/11022/0000-0007-D8A6-A

# License

https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode

# Citation

Please Cite as:

Hedeland, H. & Ferger, A. (forthcoming) Towards Continuous Quality Control for Spoken Language Corpora. In: International Journal of Digital Curation (IJDC). 